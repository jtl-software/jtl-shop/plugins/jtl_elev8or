{block name='snippets-scroll-top' append}
    <script>
        window.onload = function () {
            var elevator = new Elevator({
                element: document.querySelector('.smoothscroll-top'),
                mainAudio: '{$ShopURLSSL}/plugins/jtl_elev8or/frontend/music/elevator.mp3',
                endAudio: '{$ShopURLSSL}/plugins/jtl_elev8or/frontend/music/ding.mp3'
            });
        }
    </script>
{/block}
